---
title: "Loud Campeã do Champions 2022"
date: 2021-09-18T16:59:34-03:00
draft: false
layout: "LOUD"
---
|         |                                                     |                             |
|---------|-----------------------------------------------------|-----------------------------|
| 1°      | LOUD                                                | US$ 300 mil (R$ 1,5 milhão) |
| 2°      | OpTic Gaming                                        | US$ 150 mil (R$ 750 mil)    |
| 3°      | DRX                                                 | US$ 110 mil (R$ 550 mil)    |
| 4°      | FunPlus Phoenix                                     | US$ 80 mil (R$ 400 mil)     |
| 5°–6°   | XSET e Fnatic                                       | US$ 60 mil (R$ 300 mil)     |
| 7°–8°   | Leviatán e Team Liquid                              | US$ 40 mil (R$ 200 mil)     |
| 9°–12°  | Paper Rex, ZETA DIVISION, 100 Thieves e KRÜ Esports | US$ 25 mil (R$ 125 mil)     |
| 13°–16° | FURIA Esports, EDward Gaming, BOOM Esports e XERXIA | US$ 15 mil (R$ 75 mil)      |